package uz.baxtiyor.webserver;

import io.vertx.core.AbstractVerticle;
public class MainVerticle extends AbstractVerticle {
    @Override
    public void start() {

        vertx.createHttpServer()
                .requestHandler(
                        routerContext->routerContext
                        .response()
                        .end("Assalomu alaykum ustoz")
                )
                .listen(8080);
    }
}
